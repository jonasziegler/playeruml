package at.zie.Player_UML;

public class Song implements Playables{
	private String name;
	private int length;
	
	public Song(String name, int length) {
		super();
		this.name = name;
		this.length = length;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Song: '" + this.getName() + "' wurde abgespielt");
	}
	
	
}
