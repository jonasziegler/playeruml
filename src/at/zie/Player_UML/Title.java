package at.zie.Player_UML;

public class Title implements Playables{
	private String name;
	
	public Title(String name) {
		super();
		this.name = name;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Titel: '" + this.getName() + "' wurde abgespielt");
	}

	
	
	
}
