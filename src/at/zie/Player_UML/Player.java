package at.zie.Player_UML;

import java.util.ArrayList;
import java.util.List;

public class Player {
	private List<Playables> mediaList;
	
	public Player() {
		super();
		this.mediaList = new ArrayList<Playables>();
	}
	public void playAll() {
		for (Playables playables : mediaList) {
			playables.play();
		}
	}
	
	public void addMedia(Playables mediaName){
		mediaList.add(mediaName);
	}
}
