package at.zie.Player_UML;

import java.util.ArrayList;
import java.util.List;

public class DvD implements Media{
	private String name;
	private List<Title> titleList;
	
	public DvD(String name) {
		super();
		this.name = name;
		this.titleList = new ArrayList<Title>();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public void addTitle(Title titleName) {
		titleList.add(titleName);
	}
	@Override
	public void loadMediaContent(Player playerName) {
		// TODO Auto-generated method stub
		for (Title title : titleList) {
			playerName.addMedia(title);
		}
	}
}


