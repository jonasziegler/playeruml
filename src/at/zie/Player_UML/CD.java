package at.zie.Player_UML;

import java.util.ArrayList;
import java.util.List;

public class CD implements Media{
	private String name;
	private List<Song> songList;

	public CD(String name) {
		super();
		this.name = name;
		this.songList = new ArrayList<Song>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void addSong(Song songName) {
		songList.add(songName);
	}

	@Override
	public void loadMediaContent(Player playerName) {
		// TODO Auto-generated method stub
		for (Song song : songList) {
			playerName.addMedia(song);
		}
	}
	
}
