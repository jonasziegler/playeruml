package at.zie.Player_UML;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Song s1 = new Song("Primal", 3);
		Song s2 = new Song("Cold", 2);
		Song s3 = new Song("Make it", 3);
		Title t1 = new Title("Marvel");
		Title t2 = new Title("DC");
		CD c1 = new CD("Hits");
		Player p1 = new Player();
		
		c1.addSong(s1);
		c1.addSong(s2);
		c1.addSong(s3);
		
		c1.loadMediaContent(p1);
		
		p1.playAll();
	}
}
